﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{ 
    public class MongoDBInitializer : IDbInitializer
    {
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private readonly IMongoCollection<Customer> _customerCollection;
        public MongoDBInitializer(IMongoCollection<PromoCode> promoCodeCollection,
            IMongoCollection<Preference> preferenceCollection,
            IMongoCollection<Customer> customerCollection)
        {
            _customerCollection = customerCollection;
            _preferenceCollection = preferenceCollection;
            _promoCodeCollection = promoCodeCollection;
        }
        public void InitializeDb()
        {
            _promoCodeCollection.DeleteMany(_ => true);
            _preferenceCollection.DeleteMany(_ => true);
            _customerCollection.DeleteMany(_ => true);

            _preferenceCollection.InsertMany(FakeDataFactory.Preferences);
            _customerCollection.InsertMany(FakeDataFactory.Customers);
        }
    }
}
