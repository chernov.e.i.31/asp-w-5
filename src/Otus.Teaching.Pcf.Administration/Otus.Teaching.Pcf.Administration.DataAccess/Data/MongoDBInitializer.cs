﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDBInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _employeeCollection;
        private readonly IMongoCollection<Role> _roleCollection;
        public MongoDBInitializer(IMongoCollection<Employee> employeeCollection,
            IMongoCollection<Role> roleCollection)
        {
            _employeeCollection = employeeCollection;
            _roleCollection = roleCollection;
        }
        public void InitializeDb()
        {
            _employeeCollection.DeleteMany(_ => true);
            _roleCollection.DeleteMany(_ => true);

            _employeeCollection.InsertMany(FakeDataFactory.Employees);
            _roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}
